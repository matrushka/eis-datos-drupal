<?php
/**
 * @file
 * dm_indicator.features.inc
 */

/**
 * Implements hook_node_info().
 */
function dm_indicator_node_info() {
  $items = array(
    'indicator' => array(
      'name' => t('Indicator'),
      'base' => 'node_content',
      'description' => t('An indicator is used to present evaluation and can link to supporting tables and documents.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
