<?php
/**
 * @file
 * dm_infographic.features.inc
 */

/**
 * Implements hook_node_info().
 */
function dm_infographic_node_info() {
  $items = array(
    'infographic' => array(
      'name' => t('Infographic'),
      'base' => 'node_content',
      'description' => t('Infographic image that can be linked to an indicator'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
