<?php
/**
 * @file
 * dm_infographic.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function dm_infographic_taxonomy_default_vocabularies() {
  return array(
    'evaluation_tree' => array(
      'name' => 'Evaluation tree',
      'machine_name' => 'evaluation_tree',
      'description' => 'Subjects and indicators tree',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
