<?php
/**
 * @file
 * dm_table.features.inc
 */

/**
 * Implements hook_node_info().
 */
function dm_table_node_info() {
  $items = array(
    'table' => array(
      'name' => t('Table'),
      'base' => 'node_content',
      'description' => t('Data table'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
