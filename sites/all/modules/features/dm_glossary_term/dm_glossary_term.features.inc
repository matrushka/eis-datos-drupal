<?php
/**
 * @file
 * dm_glossary_term.features.inc
 */

/**
 * Implements hook_node_info().
 */
function dm_glossary_term_node_info() {
  $items = array(
    'glossary_term' => array(
      'name' => t('Glossary term'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Term'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
