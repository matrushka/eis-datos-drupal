<?php
/**
 * @file
 * dm_glossary_term.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function dm_glossary_term_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-glossary_term-field_definition'.
  $field_instances['node-glossary_term-field_definition'] = array(
    'bundle' => 'glossary_term',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_definition',
    'label' => 'Definition',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => -4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Definition');

  return $field_instances;
}
