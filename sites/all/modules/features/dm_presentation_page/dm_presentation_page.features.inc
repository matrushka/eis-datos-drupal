<?php
/**
 * @file
 * dm_presentation_page.features.inc
 */

/**
 * Implements hook_node_info().
 */
function dm_presentation_page_node_info() {
  $items = array(
    'presentation_page' => array(
      'name' => t('Presentation page'),
      'base' => 'node_content',
      'description' => t('Presentation page for an entity or a group of indicators'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
