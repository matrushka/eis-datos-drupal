<?php

/**
 * @file
 * TODO: Enter file description here.
 */

/**
 * Form builder.
 */
function evaluation_tree_preview_settings_form($form, &$form_state) {
  $form['evaluation_tree_preview_evaluated_entities_vocabulary'] = array(
    '#type' => 'textfield',
    '#title' => t('Evaluated entities (i.e. Countries) vocabulary name'),
    '#default_value' => variable_get('evaluation_tree_preview_evaluated_entities_vocabulary', 'evaluated_entities'),
    '#description' => t('Enter the machine name for the evaluated entities vocabulary, i.e. <em>countries</em>'),
    '#required' => TRUE,
  );

  $form['evaluation_tree_preview_evaluation_tree_vocabulary'] = array(
    '#type' => 'textfield',
    '#title' => t('Evaluation tree vocabulary name'),
    '#default_value' => variable_get('evaluation_tree_preview_evaluation_tree_vocabulary', 'evaluation_tree'),
    '#description' => t('Enter the machine name for the evaluation tree vocabulary, i.e. <em>evaluation_tree</em>'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
