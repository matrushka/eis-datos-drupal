<h2><?php print $evaluated_item->name; ?></h2>

<?php if(!empty($evaluated_item->nodes)): ?>
<div class="item-nodes">
<?php foreach($evaluated_item->nodes as $node) {
  print drupal_render($node);
} ?>
</div>
<?php else:
  if(!empty($evaluated_item->field_tree_content_type)) {
    $content_type = str_replace('_', '-', $evaluated_item->field_tree_content_type['und'][0]['value']);
    $evaluated_item_tid = $evaluated_item->tid;
    $evaluated_entity_name = $evaluated_entity_term->name;
    $create_path = "node/add/$content_type/$evaluated_entity_name/$evaluated_item_tid";
    print l('crear ' . $content_type, $create_path, array(
      'attributes' => array(
        'class' => 'create-item', 
      ),
      'query' => array(
        'destination' => current_path()
      )
    ));
  }
endif;
?>

<?php if(!empty($evaluated_item->children)):
$children = $evaluated_item->children;
?>
<ul>
  <?php foreach($children as $item):
  ?>
  <li><?php print theme('evaluation_item', array(
    'evaluated_item' => $item,
    'evaluated_entity_term' => $evaluated_entity_term
  )); ?> </li>
  <?php endforeach; ?>
</ul>
<?php endif; ?>
