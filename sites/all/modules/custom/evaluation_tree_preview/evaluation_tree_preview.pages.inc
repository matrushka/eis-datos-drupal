<?php

function etp_tree_list() {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_taxonomy');
  
  $vocabulary_name = variable_get('srrl_cache_countries_vocabulary');
  $tree = scc_get_vocabulary_tree_by_name($vocabulary_name);
  
  $items = array();

  foreach($tree as $country) {
    $items[] = l($country[ 'name' ], 'preview/country/' . $country[ 'tid' ]);
  }

  return theme('item_list', array('items' => $items));
}

function etp_preview($entity_tid) {
  module_load_include('inc', 'matrushka_helper', 'matrushka_helper.taxonomy');

  $vocabulary_name = variable_get('evaluation_tree_preview_evaluation_tree_vocabulary', 'evaluation_tree');
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name);

  if(empty($vocabulary)) {
    watchdog('evaluation_tree_preview', 'The vocabulary configured for the the evaluation tree does not exist', null, WATCHDOG_ERROR);
    return 'Error loading evaluation tree. See log messages.';
  }

  $tree = taxonomy_get_tree($vocabulary->vid);
  mh_load_full_terms($tree);
  etp_populate_tree($tree, $entity_tid);
  $tree_nested = mh_taxonomy_get_nested_tree($tree);

  $output = '';
  $evaluated_entity_term = taxonomy_term_load(arg(1));
  foreach($tree_nested as $item) {
    $output .= theme('evaluation_item', array(
      'evaluated_item' => $item,
      'evaluated_entity_term' => $evaluated_entity_term
    ));
  }

  return $output;
}

/**
 * Load the corresponding node for each evaluation item, if exists
 * 
 * @param array &$flat_tree
 *   An array of terms, as returned by taxonomy_get_tree
 *
 * @param int $entity_tid
 *   The taxonomy term TID of the entity being evaluated (i.e. Country). Not a Drupal entity in any way.
 *
 */
function etp_populate_tree(&$flat_tree, $entity_tid) {
  foreach($flat_tree as &$item) {
    $item_nid = etp_get_evaluation_item_nid($item->tid, $entity_tid);
    $item->nodes = array();
    if($item_nid) {
      $nodes = node_load_multiple($item_nid);
      foreach($nodes as $node) {
        $node_render_array = node_view($node);
        $item->nodes[$node->nid] = $node_render_array;
      }
    }
  }
}

/**
 * Get the NID of the node corresponding to an evaluation item for a particular evaluated entity
 * 
 * @param int $evaluated_item_tid
 *   The TID of the evaluated item term
 *
 * @param int $evaluated_entity_tid
 *   The TID of the evaluated entity term
 *
 */
function etp_get_evaluation_item_nid($evaluated_item_tid, $evaluated_entity_tid) {
  // TODO: add field configuration options in admin page for the field names
  // TODO: add documentation for creating content types
  $evaluated_entity_tid_field = variable_get('etp_entity_tid_field', 'field_evaluated_entity');
  $evaluated_item_tid_field = variable_get('etp_evaluation_tid_field', 'field_evaluated_item');

  $sql = <<<SQL
SELECT n.nid FROM
  {node} as n
  LEFT JOIN {field_data_$evaluated_entity_tid_field} as c
    ON c.entity_id = n.nid
  LEFT JOIN {field_data_$evaluated_item_tid_field} as t
    ON t.entity_id = n.nid
WHERE c.{$evaluated_entity_tid_field}_tid = :entity_tid
AND t.{$evaluated_item_tid_field}_tid = :item_tid
SQL;
  
  $result = db_query($sql, array(':entity_tid' => $evaluated_entity_tid, ':item_tid' => $evaluated_item_tid));
  $rows = $result->rowCount();
  $column = $result->fetchCol();

  if(!$rows) {
    return false;
  }
  else {
    return $column;
  }
}

function etp_title() {
  $evaluated_entity_term = taxonomy_term_load(arg(1));
  return $evaluated_entity_term->name;
}

