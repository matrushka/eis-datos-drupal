<?php

/**
 * Get a nested taxonomy tree from the flat one returned by taxonomy_get_tree.
 * 
 * @param array $flat_tree
 *   An array of terms, as returned by taxonomy_get_tree
 * @param array $state
 *   The argument used for recursion.
 *
 */
function mh_taxonomy_get_nested_tree($flat_tree, $state=null) {
  // If the function is just being called, initialize the state
  if(empty($state)) {
    $max_depth = array_reduce($flat_tree, function($carry, $item) {
      return max($carry, $item->depth);
    });
    
    // If the list only has one level, just return it
    if($max_depth == 0) {
      return $flat_tree;
    }

    $state = array(
      'max_depth' => $max_depth,
      'current_depth' => $max_depth,
      'tree' => array()
    );
  }
  else {
    // Go to the parent generation
    --$state['current_depth'];
  }

  $current_depth_items = array_filter($flat_tree, function($value) use ($state) {
    return $value->depth == $state['current_depth'];
  });
  
  // If we are at a depth lower than max_depth, there's already of grouped descendants
  // to integrate as children of the current generation. Do it.
  if($state['current_depth'] < $state['max_depth']) {
    // $generation_descendants should contain the elements at a level deeper
    // than current level, grouped by parent TID
    $generation_descendants = $state['generation_descendants'];

    foreach($current_depth_items as &$generation_member) {
      $tid = $generation_member->tid;

      if(!empty($generation_descendants[$tid])) {
        $generation_member->children = $generation_descendants[$tid];
      }
    }
  }

  // If the current depth is 0, just return the current generation of elders
  if($state['current_depth'] == 0) {
    return $current_depth_items;
  }
  
  // Now that the current generation was reunited with their children, 
  // and we know that this generation is not the eldest, group them by parents
  // to prepare for another iteration
  $grouped_generation = array();
  foreach($current_depth_items as &$generation_member) {
    $parents = $generation_member->parents;
    $own_tid = $generation_member->tid;

    foreach($parents as $parent_tid) {
      if($own_tid != $parent_tid) {
        $grouped_generation[$parent_tid][] = $generation_member;
      }
    }
  }
  
  // Call again to go to the parent generation
  $state['generation_descendants'] = $grouped_generation;
  return mh_taxonomy_get_nested_tree($flat_tree, $state);
}

/**
 * Replace each of the evaluation items with the full taxonomy term
 * 
 * @param array &$flat_tree
 *   An array of terms, as returned by taxonomy_get_tree
 *
 */
function mh_load_full_terms(&$flat_tree) {
  foreach($flat_tree as &$item) {
    $tid = $item->tid;
    $full_term = taxonomy_term_load($tid);

    // Add all the properties from the full term to the item
    foreach($full_term as $prop => $value) {
      $item->$prop = $value;
    }
  }
}
