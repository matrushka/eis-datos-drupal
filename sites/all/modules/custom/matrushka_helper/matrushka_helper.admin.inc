<?php

/**
 * @file
 * TODO: Enter file description here.
 */

/**
 * Form builder.
 */
function matrushka_taxonomy_settings_form($form, &$form_state) {
  $form['matrushka_taxonomy_variable_foo'] = array(
    '#type' => 'textfield',
    '#title' => t('Foo'),
    '#default_value' => variable_get('matrushka_taxonomy_variable_foo', 42),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
