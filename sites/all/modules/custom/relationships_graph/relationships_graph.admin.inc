<?php

/**
 * @file
 * TODO: Enter file description here.
 */

/**
 * Form builder.
 */
function relationships_graph_settings_form($form, &$form_state) {
  $form['relationships_graph_variable_foo'] = array(
    '#type' => 'textfield',
    '#title' => t('Foo'),
    '#default_value' => variable_get('relationships_graph_variable_foo', 42),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
