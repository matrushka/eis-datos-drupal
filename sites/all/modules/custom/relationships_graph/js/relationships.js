(function($) {
  $(function() {
    var width=600,
        height=400;

    var color = d3.scale.category20();

    var force = d3.layout.force()
        .charge(-120)
        .linkDistance(30)
        .size([width, height]);

    var svg = d3.select("div#graph").append("svg")
        .attr("width", width)
        .attr("height", height);
    
    var graphData = Drupal.settings.relationships_graph;

    var nodes = [];
    var nodesIndex = {};
    for(nodeId in graphData.nodes) {
      nodes.push(graphData.nodes[nodeId]);
      nodesIndex[nodeId] = nodes.length - 1;
    }

    var links = graphData.links;
    for(var i=0; i<links.length; i++) {
      var sourceIndex = nodesIndex[links[i]['source']];
      var targetIndex = nodesIndex[links[i]['target']];

      links[i]['source'] = nodes[sourceIndex];
      links[i]['target'] = nodes[targetIndex];
    }

    force.nodes(nodes)
         .links(links)
         .linkDistance(100)
         .gravity(0.02)
         .linkStrength(0.1)
         .start();
    
    svg.append('svg:defs').selectAll('marker')
      .data(["end"])
    .enter().append("svg:marker")
      .attr("id", String)
      .attr("viewBox", "0 -5 10 10")
      .attr("refX", -15)
      .attr("refY", 0)
      .attr("markerWidth", 10)
      .attr("markerHeight", 10)
      .attr("orient", "auto")
    .append("svg:path")
      .attr("d", "M10,-5L0,0L10,5");

    // svg.select("defs").append('div')
    //   .attr("id", "filters")
    //   .html('<filter id="shadow" x="-20%" y="-20%" width="140%" height="140%">' +
    //         '<feGaussianBlur stdDeviation="2 2" result="shadow"/>' +
    //         '<feOffset dx="1" dy="1"/>' +
    //         '</filter>')

    var link = svg.selectAll(".link")
      .data(links)
    .enter().append("line")
      .attr("class", "link")
      .attr("marker-start", "url(#end)")
      //.style("stroke-width", function(d) { return Math.sqrt(d.value); });

    var groups = svg.selectAll("g.gnode")
      .data(nodes)
    .enter().append("g")
      .classed("gnode", true)

    var node = groups.append("circle")
      .attr('class', 'node')
      .attr("r", 10)
      .style("fill", function(d) { return (d.type == 'content_type') ? '#E6F5B3' : '#90C3D4'; })
      .call(force.drag);

    var text = groups.append("text")
              .text(function(d) {
                return d.name;
              })
              .call(force.drag);

    // var textShadows = groups.append("text")
    //           .text(function(d) {
    //             return d.name;
    //           })
    //           .attr('class', 'shadow');

    force.on("tick", function() {
      link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });

      node.attr("cx", function(d) { return d.x; })
          .attr("cy", function(d) { return d.y; });

      text.attr("x", function(d) { return d.x; })
          .attr("y", function(d) { return d.y; });

      // textShadows.attr("x", function(d) { return d.x; })
      //     .attr("y", function(d) { return d.y; });
  });
  });
})(jQuery);
