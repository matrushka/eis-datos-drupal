<?php

function relationships_graph_page() {
  $content_types = _relationships_graph_get_content_types();
  $vocabularies = _relationships_graph_get_vocabularies();

  $nodes = array_merge($content_types, $vocabularies);
  $links = _relationships_graph_get_field_links($nodes);
  
  _load_d3_js();
  drupal_add_js(array(
    'relationships_graph' => array(
      'links' => $links,
      'nodes' => $nodes
    )
  ), 'setting');

  $path = drupal_get_path('module', 'relationships_graph');
  drupal_add_js("$path/js/relationships.js"); 
  drupal_add_css("$path/css/relationships.css"); 

  return '<div id="graph"></div>';
}

function _load_d3_js() {
  $path = "sites/all/libraries/d3";
  drupal_add_js("$path/d3.min.js");
}

function _relationships_graph_get_field_links($nodes) {
  $links = array();

  foreach($nodes as $id => $node) {
    if($node['type'] == 'content_type') {
      // If this is a content type, scan fields to find references to
      // other content types or to taxonomy vocabularies
      foreach($node['obj']->fields as $field_instance) {
        $field = field_info_field($field_instance['field_name']);
        $module = $field['module'];

        if($module == 'taxonomy') {
          $vocabulary_name = $field['settings']['allowed_values'][0]['vocabulary'];
          $links[] = array(
            'source' => 'type-' . $node['type_code'], 
            'predicate' => 'isTaggedBy',
            'target' => 'vocabulary-' . $vocabulary_name,
            'value' => 5
          );

        }
        else if($module == 'entityreference') {
          // dpm('este es ER: ');
          // dpm($field);
          // dpm($field_instance);
         
          // If target bundles weren't set, then this content type can
          // potentially link to all other types
          if(empty($field['settings']['handler_settings']['target_bundles'])) {
            $all_types = node_type_get_types();
            foreach($all_types as $linked_type) {
              $links[] = array(
                'source' => 'type-' . $node['type_code'], 
                'predicate' => 'mayHave',
                'target' => 'type-' . $linked_type->type,
                'value' => 3
              );
            }
          }
          else {
            foreach($field['settings']['handler_settings']['target_bundles'] as $target_bundle) {
              $links[] = array(
                'source' => 'type-' . $node['type_code'], 
                'predicate' => 'mayHave',
                'target' => 'type-' . $target_bundle,
                'value' => 5
              );
            }
          }
        }
      }
    }
  }

  return $links;
}

function _relationships_graph_get_content_types() {
  $nodes = array();
  $content_types = node_type_get_types();
  
  foreach($content_types as &$type) {
    $type->fields = field_info_instances('node', $type->type); 
    $nodes['type-' . $type->type] = array(
      'name' => $type->name,
      'type' => 'content_type',
      'type_code' => $type->type,
      'obj' => $type
    );
  }

  return $nodes;
}

function _relationships_graph_get_vocabularies() {
  $nodes = array();
  $vocabularies = taxonomy_get_vocabularies();

  foreach($vocabularies as $vocabulary) {
    $nodes['vocabulary-' . $vocabulary->machine_name] = array(
      'name' => $vocabulary->name,
      'type' => 'taxonomy',
      'vid' => $vocabulary->vid,
      'obj' => $vocabulary
    );
  }

  return $nodes;
}



// Get content types
// Get vocabularies
// For each content type check
// —if it has ER fields that point to another ct
// -if it has TR fields that point to taxonomy

// and create a link for each of them

?>
